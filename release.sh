#!/bin/bash

# Usage:
# If you want to release a new version, make sure all changes are committed.
# Then, create a tag (f.e.: v0.3) to name the current release. If you just
# want a development snapshot, you can just invoke this script; the name
# of the package will reflect its relation to the last official release.
# If you made some uncommitted local modifications, '-local' will be
# appended to the version string.
#
# Simply invoke ./release.sh and you will find a new (bzip2 compressed)
# tarball in RELEASE_DIR (typically set to 'snapshots'). Call this script
# from anywhere within a git repository.
#
# Name and settings will be read from a file named .release_cfg (in top-level).
# If it doesn't exist, it will be created with sane defaults that can be adapted.

DEFAULT_RELEASE_DIR=snapshots
CONFIG_NAME=.release_cfg
SED_TRANSFORM_VERSION=
NAME_SEPARATOR="_"
EMPTY_NAME=0
INCLUDE_GIT=0

_version() {
	VERSION="0.3"

	echo "$(basename "$0") version $VERSION"
}

_exit() {
	echo "$@"
	exit 1
}

_cleanup_tmp() {
	rm "${exclude_file}"
}

_tar() {
	local rname=$1
	local ret=

	exclude_file=$(mktemp -t "release_exclude.XXXXXXXXXXXXXX") || return 1
	trap _cleanup_tmp EXIT

	echo "${RELEASE_DIR}" > "${exclude_file}"
	echo "${CONFIG_NAME}" >> "${exclude_file}"
	if [ ${INCLUDE_GIT} -eq 0 ]; then
		echo ".git" >> "${exclude_file}"
	fi

	tar cj --exclude-from="${exclude_file}"  --transform "s/^./${rname}/" . > ${RELEASE_DIR}/${rname}.tar.bz2
	return $?
}

read_config() {
	local top_dir="$(git rev-parse --show-toplevel)"
	local create_cfg=1
	if [ -f "${top_dir}/${CONFIG_NAME}" ]; then
		create_cfg=0
		source "${top_dir}/${CONFIG_NAME}"
	fi

	[ ! -z "${RELEASE_DIR}" ] || RELEASE_DIR="${DEFAULT_RELEASE_DIR}"
	if [ ${EMPTY_NAME} -eq 0 ]; then
		[ ! -z "${NAME}" ] || NAME="$(basename "${top_dir}")"
	fi

	if [ ${create_cfg} -eq 1 ]; then
		local new_cfg="${top_dir}/${CONFIG_NAME}"
		echo "RELEASE_DIR=\"${RELEASE_DIR}\"" > "${new_cfg}"
		echo "NAME=\"${NAME}\"" >> "${new_cfg}"
	fi

	TOP_DIR="${top_dir}"

	return 0
}

_usage() {
	echo "usage: $0 [-f] [-C directory] [-O directory]"
	echo "  -f            Include .git directory in this release"
	echo "  -C directory  Change to this directory before releasing"
	echo "  -O directory  Write snapshot to this directory"
	echo ""
}

parse_args() {
	g_change_dir=
	g_include_git=
	g_output_dir=
	while getopts "fhVC:O:" opt; do
		case $opt in
		f)
			echo "Release full git archive"
			g_include_git=1
			;;

		h)
			_usage
			exit 0
			;;

		V)
			_version
			exit 0
			;;

		C)
			echo "Change to directory '${OPTARG}'"
			g_change_dir="${OPTARG}"
			;;

		O)
			echo "Write Output to '${OPTARG}'"
			g_output_dir="${OPTARG}"
			;;

		\?)
			return 1
			;;

		:)
			echo "Option ${OPTARG} requires an argument" >&2
			return 1
			;;
		esac
	done

	return 0
}

main() {
	local version
	local release

	if ! parse_args $@ ; then
		echo "Parsing arguments failed! '$@'" >&2
		_usage >&2
		exit 1
	fi

	if [ ! -z "${g_change_dir}" ]; then
		cd "$g_change_dir" || _exit "Couldn't change to directory '${g_change_dir}'"
	fi

	read_config

	[ -z "${g_include_git}" ] || INCLUDE_GIT=${g_include_git}
	[ -z "${g_output_dir}" ] || RELEASE_DIR=${g_output_dir}

	cd "${TOP_DIR}" || _exit "Couldn't change to toplevel directory '${TOP_DIR}'"
	mkdir -p "${RELEASE_DIR}" || _exit "Couldn't create RELEASE_DIR ('${RELEASE_DIR}')"

	version="$(git describe --tags --dirty=-local)" || _exit "git describe failed!"
	if [ ! -z ${SED_TRANSFORM_VERSION} ]; then
		version="$(echo "${version}" | sed -e "${SED_TRANSFORM_VERSION}")"
	fi
	release="${NAME}${NAME_SEPARATOR}${version}"

	echo "Creating tarball of version ${version} ... "
	_tar "${release}" || _exit "Creating tarball failed!"

	echo "Successfully released '${release}'"

	return 0
}

main $@
